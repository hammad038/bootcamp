// imports pckgs
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');

// import files & others
dotenv.config({ path: './config/.env'});
const errorHandler = require('./middleware/error');
const connectDB = require('./config/db');
const PORT = process.env.PORT || 5000;

// connect database
connectDB();

// routes files
const bootcamp = require('./api/v1/bootcamp/bootcamp');

// init express app
const app = express();

// body-parser
app.use(express.json());

// middleware
app.use(morgan('dev'));

// api routes
app.use('/api/v1/bootcamp', bootcamp);

// error handler
app.use(errorHandler);

// start server
const server = app.listen(
    PORT,
    () => console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}.`)
);

// unhandled rejections
process.on('unhandledRejection', (error, promise) => {
  console.log(`Error: ${error.message}`);

  // close server and exit
  server.close(() => {
    process.exit(1);
  });
});
