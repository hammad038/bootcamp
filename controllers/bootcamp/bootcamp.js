// import files & others
const ErrorResponse = require('../../util/errorResponse');
const asyncHandler = require('../../middleware/async');
const geocoder = require('../../util/geocoder');

// load models
const Bootcamp = require('../../models/Bootcamp');

// @route   GET /api/v1/bootcamp/
// @desc    get all bootcamps
// @access  public
exports.getBootcamps = asyncHandler(async (req, res, next) => {
  let query;
  let reqQuery = { ...req.query };
  const removeFields = ['select', 'sort', 'limit', 'page'];

  // delete keywords from fields query
  removeFields.forEach(param => delete reqQuery[param]);

  let queryString = JSON.stringify(reqQuery);

  // create operators query string
  queryString = queryString.replace(/\b(gt|gte|lt|lte|in)\b/g, match => `$${match}`);

  // finding resources
  query = Bootcamp.find(JSON.parse(queryString));

  // select fields
  if(req.query.select) {
    const fields = req.query.select.split(',').join(' ');
    query = query.select(fields);
  }

  // sort
  if(req.query.sort) {
    const sortBy = req.query.sort.split(',').join(' ');
    query = query.sort(sortBy);
  } else {
    query.sort('-createdAt');
  }

  // pagination
  const page = parseInt(req.query.page, 10 || 1);
  const limit = parseInt(req.query.limit, 10 || 25);
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const total = await Bootcamp.countDocuments();

  query = query.skip(startIndex).limit(limit);

  const bootcamps = await query;

  const pagination = {};

  if(endIndex < total) {
    pagination.next = {
      page: page +1,
      limit
    }
  }

  if(startIndex > 0) {
    pagination.prev = {
      page: page -1,
      limit
    }
  }

  res
    .status(200)
    .json({
      success: true,
      count: bootcamps.length,
      pagination,
      data: bootcamps
    });

});

// @route   GET /api/v1/bootcamp/:id
// @desc    get bootcamp with :id
// @access  private
exports.getBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);

  if(!bootcamp) {
    return next(new ErrorResponse(`Bootcamp not found with id: ${req.params.id}`, 404));
  }

  res
    .status(200)
    .json({ success: true, data: bootcamp });
});

// @route   POST /api/v1/bootcamp/
// @desc    create a new bootcamp
// @access  private
exports.createBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.create(req.body);

  res
    .status(201)
    .json({ success: true, data: bootcamp });
});

// @route   PUT /api/v1/bootcamp/:id
// @desc    update a bootcamp with :id
// @access  private
exports.updateBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if(!bootcamp) {
    return next(new ErrorResponse(`Bootcamp not found with id: ${req.params.id}`, 404));
  }

  res
    .status(200)
    .json({ success: true, data: bootcamp });
});

// @route   DELETE /api/v1/bootcamp/:id
// @desc    delete a bootcamp with :id
// @access  private
exports.deleteBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findByIdAndDelete(req.params.id);

  if(!bootcamp) {
    return next(new ErrorResponse(`Bootcamp not found with id: ${req.params.id}`, 404));
  }

  res
    .status(200)
    .json({ success: true, data: {} });
});

// @route   GET /api/v1/bootcamp/radius/:zipcode/:distance
// @desc    get bootcamps within a radius
// @access  private
exports.getBootcampsInRadius = asyncHandler(async (req, res, next) => {
  const { zipcode, distance } = req.params;

  // get lat/lang from geocoder
  const loc = await geocoder.geocode(zipcode);
  const lat = loc[0].latitude;
  const lng = loc[0].longitude;
  const earthRadiusInMiles = 3963;

  // calculate radius using radians
  // divide distance by radius of earth
  // earth radius: 3,963 mi / 6,378 km

  const radius = distance / earthRadiusInMiles;
  console.log(zipcode, distance, lat, lng, earthRadiusInMiles, radius);

  const bootcamps = await Bootcamp.find({
    location: { $geoWithin: { $centerSphere: [[lng, lat], radius] } }
  });

  res
    .status(200)
    .json({
      success: true,
      count: bootcamps.length,
      data: bootcamps
    });
});
