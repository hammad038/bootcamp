// imports pckgs
const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

// import files & others
dotenv.config({ path: './config/.env' });

// load models
const Bootcamp = require('./models/Bootcamp');

// connect to db
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});

// read json file
const bootcamps = JSON.parse(fs.readFileSync(`${__dirname}/_data/bootcamps.json`, 'utf-8'));

// import data into db
const importData = async () => {
  try {
    await Bootcamp.create(bootcamps);

    console.log('data imported...');
    process.exit();
  } catch (err) {
    console.log(err);
  }
}

// delete data from db
const deleteData = async () => {
  try {
    await Bootcamp.deleteMany();

    console.log('data destroyed...');
    process.exit();
  } catch (err) {
    console.log(err);
  }
}

if(process.argv[2] === '-i') {
  importData();
} else if(process.argv[2] === '-d') {
  deleteData();
}
